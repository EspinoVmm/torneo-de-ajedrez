create DATABASE ProyectoAjedrez;
show tables;


#region hotel
create table Hotel(
idHotel int primary key auto_increment not null,
Nombre varchar(40), 
Direccion varchar(40),
Telefono varchar(10)
);
#endregion
# region Sala
create table Sala(
idsala int primary key auto_increment not null, 
Codigo_Sala varchar(40),
medio varchar(40),
Capacidad int,
fkidHotel int, 
FOREIGN KEY (FkidHotel) REFERENCES Hotel (idHotel)
);
#endregion
#region Pais
create table Pais(
idPais int primary key auto_increment not null,
Pais varchar (60),
Nombre varchar(40),
N_clubs int
);
#endregion
#region participante
create table Participante(
idParticipante int primary key auto_increment not null,
N_social int, 
Nombre varchar(40),
direcion varchar(40),
NombreCampeonato varchar(40),
tipoCampeonato varchar(40),
telefono int,
fkidPais int,
FOREIGN KEY (fkidPais) REFERENCES Pais (idPais)
);
#endregion
#region alojamieto
create table Aloja(
idAloja int primary key auto_increment not null,
fecha_entrada date,
fecha_salida date,
fkidParticipante int ,
fkidhotel int,
FOREIGN KEY (fkidParticipante) REFERENCES Participante (idParticipante),
FOREIGN KEY (fkidHotel) REFERENCES Hotel (idHotel)
);
#endregion
#region jugador
drop table Jugador;
select * from Jugador;
create table Jugador(
Id_Jugador int primary key  AUTO_INCREMENT  not null,
Nivel varchar(40),
fkidParticipante int,
FOREIGN KEY (fkidParticipante) REFERENCES Participante (idParticipante)
);
#endregion
#region albitro
create table Albitro(
id_Albrito int primary key  AUTO_INCREMENT not null ,
Nombre varchar(50),
fkidParticipante int,
FOREIGN KEY (fkidParticipante) REFERENCES Participante (idParticipante)
);
#endregion
#region partida
create table Partida(
idPartida int primary key auto_increment not null,
Codigo_Partida int,
Dia int,
mes int,
a�o int,
fkidsala int,
FOREIGN KEY (fkidSala) REFERENCES Sala (idSala)
);
#endregion
#region movimiento
create table Movimiento(
id_Movimiento int primary key  AUTO_INCREMENT not null,
jugada int,
Movimiento int,
Comentario varchar(100),
fk_idPartida int,
FOREIGN KEY (fk_idPartida) REFERENCES  Partida(idPartida)
);
#endregion
#region campeonato

#endregion
#region juego

#endregion