﻿using System;
using System.Windows.Forms;
using LogicaNegocio;
using EntdadesAjedrez;

namespace Presentación
{
    public partial class FrmPartida : Form
    {
        private Partida _partida;
        private PartidaManejador _partidaM;
        public FrmPartida()
        {
            InitializeComponent();
            _partida = new Partida();
            _partidaM = new PartidaManejador();
        }
        private void Buscar(string nombre)
        {
            dgvPartida.DataSource = _partidaM.Obtener(nombre);
        }
        private void ActBoton(Boolean eliminar, Boolean editar)
        {
            btnEliminar.Enabled = eliminar;
            btnEditar.Enabled = editar;
        }
        private void limpiar()
        {
            txtBuscar.Text = "";
            txtModificar.Text = "";
            txtAño.Text = "";
            txtCodigoPartida.Text = "";
            txtDia.Text = "";
            txtMes.Text = "";
            txtSala.Text = "";
        }
        private void Eliminar()
        {
            var id = dgvPartida.CurrentRow.Cells["codigo_partida"].Value.ToString();
            _partidaM.Eliminar(id);
        }
        private void ActText(Boolean cpartida, Boolean dia, Boolean mes, Boolean año,Boolean sala)
        {
            txtAño.Enabled = año;
            txtCodigoPartida.Enabled = cpartida;
            txtDia.Enabled = dia;
            txtMes.Enabled = mes;
            txtSala.Enabled = sala;
        }
        private void Guardar()
        {
            Partida partida = new Partida();
            partida.Codigo_partida = int.Parse(txtCodigoPartida.Text);
            partida.Dia = int.Parse(txtDia.Text);
            partida.Mes = int.Parse(txtMes.Text);
            partida.Año = int.Parse(txtAño.Text);
            partida.Fkidsala = int.Parse(txtSala.Text);
            var valida = _partidaM.Validar(partida);
            if (valida.Item1)
            {
                _partidaM.Guardar(partida);
                MessageBox.Show("la zona sala se guardo correctamente");
            }
            else
            {
                MessageBox.Show(valida.Item2, "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void dgvPartida_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            txtModificar.Visible = true;
            lblModificar.Visible = true;
            ActBoton(true, true);
            ActText(true, false, false, false,false);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            btnNuevo.Visible = false;
            btnAgregar.Visible = true;
            ActText(false, false, false, false,false);
            Guardar();
            limpiar();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            btnNuevo.Visible = true;
            btnAgregar.Visible = false;
            ActText(true, true, true, true,true);
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            _partidaM.Actualizar(new Partida
            {
                Codigo_partida= int.Parse(txtCodigoPartida.Text),
                Idpartida= int.Parse(txtModificar.Text)
            });
            txtModificar.Visible = false;
            lblModificar.Visible = false;
            ActText(false, false, false, false,false);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar la partida actual", "Eliminar Partida", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                MessageBox.Show("Partida eliminado");
            }
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            Buscar(txtBuscar.Text);
        }
        private void Partida_Load(object sender, EventArgs e)
        {
            ActBoton(false, false);
            btnNuevo.Visible = false;
            ActText(false, false, false, false,false);
            txtModificar.Visible = false;
            lblModificar.Visible = false;
            limpiar();
        }
    }
}
