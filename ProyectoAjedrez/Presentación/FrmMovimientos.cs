﻿using System;
using System.Windows.Forms;
using LogicaNegocio;
using EntdadesAjedrez;

namespace Presentación
{
    public partial class FrmMovimientos : Form
    {
        private MovimientoManejador _movimientoM;
        private Movimiento _movimiento;
        public FrmMovimientos()
        {
            InitializeComponent();
            _movimiento = new Movimiento();
            _movimientoM = new MovimientoManejador();
        }
        private void Buscar(string nombre)
        {
            dgvMovimiento.DataSource = _movimientoM.Obtener(nombre);
        }
        private void ActBoton(Boolean eliminar, Boolean editar)
        {
            btnEliminar.Enabled = eliminar;
            btnEditar.Enabled = editar;
        }
        private void limpiar()
        {
            txtComentario.Text = "";
            txtID.Text = "";
            txtIdMovimiento.Text = "";
            txtJugada.Text = "";
            txtMovimiento.Text = "";
            txtPartida.Text = "";
        }
        private void Eliminar()
        {
            var idMov = dgvMovimiento.CurrentRow.Cells["id_movimiento"].Value.ToString();
            _movimientoM.Eliminar(idMov);
        }
        private void ActText(Boolean jugada, Boolean comentario,Boolean movimiento,Boolean partida)
        {
            txtJugada.Enabled =jugada;
            txtComentario.Enabled =comentario;
            txtMovimiento.Enabled =movimiento;
            txtPartida.Enabled =partida;
        }
        private void GuardarJ()
        {
            Movimiento movimiento = new Movimiento();
            movimiento.Jugada = txtJugada.Text;
            movimiento.Movimiento1 = int.Parse(txtMovimiento.Text);
            movimiento.Comentario = txtComentario.Text;
            movimiento.Fk_idPartida = int.Parse(txtPartida.Text);
            var valida = _movimientoM.Validar(movimiento);
            if (valida.Item1)
            {
                _movimientoM.Guardar(movimiento);
                MessageBox.Show("El Movimiento se guardo correctamente");
            }
            else
            {
                MessageBox.Show(valida.Item2, "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            btnNuevo.Visible = true;
            btnAgregar.Visible = false;
            ActText(true, true,true,true);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            btnNuevo.Visible = false;
            btnAgregar.Visible = true;
            ActText(false, false,false,false);
            GuardarJ();
            limpiar();

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar el Movimiento actual", "Eliminar Movimiento", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                MessageBox.Show("Movimiento eliminado");
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            _movimientoM.Actualizar(new Movimiento
            {
                Comentario = txtComentario.Text,
                Id_movimiento = int.Parse(txtIdMovimiento.Text)
            }) ;
            txtIdMovimiento.Visible = false;
            lblMovimiento.Visible = false;
            ActText(false, false,false,false);
        }

        private void txtID_TextChanged(object sender, EventArgs e)
        {
            Buscar(txtID.Text);
        }

        private void dgvMovimiento_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            txtIdMovimiento.Visible = true;
            lblMovimiento.Visible = true;
            ActBoton(true, true);
            ActText(false,true, false,false);
        }

        private void FrmMovimientos_Load(object sender, EventArgs e)
        {
            ActBoton(false, false);
            btnNuevo.Visible = false;
            ActText(false, false,false,false);
            txtIdMovimiento.Visible = false;
            lblMovimiento.Visible = false;
            limpiar();
        }
    }
}
