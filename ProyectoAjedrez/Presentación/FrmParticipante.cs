﻿using System;
using System.Windows.Forms;
using LogicaNegocio;
using EntdadesAjedrez;

namespace Presentación
{
    public partial class FrmParticipante : Form
    {
        private ParticipanteManejador _participanteM;
        private Participante _participante;
        public FrmParticipante()
        {
            InitializeComponent();
            _participante = new Participante();
            _participanteM = new ParticipanteManejador();
        }
        private void Buscar(string nombre)
        {
            dgvParticipante.DataSource = _participanteM.Obtener(nombre);
        }
        private void ActBoton(Boolean eliminar, Boolean editar)
        {
            btnEliminar.Enabled = eliminar;
            btnEditar.Enabled = editar;
        }
        private void limpiar()
        {
            txtBuscar.Text = "";
            txtModificar.Text = "";
            txtDireccion.Text = "";
            txtNombre.Text = "";
            txtPais.Text = "";
            txtNomCam.Text = "";
            txtNSeguro.Text = "";
            txtTelefono.Text = "";
            txtTipoCamp.Text = "";
        }
        private void Eliminar()
        {
            var id = dgvParticipante.CurrentRow.Cells["Nombre"].Value.ToString();
            _participanteM.Eliminar(id);
        }
        private void ActText(Boolean seguro, Boolean nombre, Boolean direccion,Boolean telefono,Boolean ncampeonato,Boolean tcampeonato,Boolean pais)
        {
            txtDireccion.Enabled =direccion;
            txtNombre.Enabled =nombre;
            txtPais.Enabled =pais;
            txtNomCam.Enabled =ncampeonato;
            txtNSeguro.Enabled =seguro;
            txtTelefono.Enabled =telefono;
            txtTipoCamp.Enabled =tcampeonato;
        }
        private void Guardar()
        {
            Participante participante = new Participante();
            participante.Direccion = txtDireccion.Text;
            participante.FkidPais = int.Parse(txtPais.Text);
            participante.Nombre = txtNombre.Text;
            participante.NombreCampeonato = txtNomCam.Text;
            participante.N_social = txtNSeguro.Text;
            participante.Telefono = txtTelefono.Text;
            participante.TipoCampeonato = txtTipoCamp.Text;
            var valida = _participanteM.Validar(participante);
            if (valida.Item1)
            {
                _participanteM.Guardar(participante);
                MessageBox.Show("El Prticipante se guardo correctamente");
            }
            else
            {
                MessageBox.Show(valida.Item2, "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            btnNuevo.Visible = false;
            btnAgregar.Visible = true;
            ActText(false, false, false, false, false, false,false);
            Guardar();
            limpiar();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            btnNuevo.Visible = true;
            btnAgregar.Visible = false;
            ActText(true, true, true, true, true, true,true);
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            _participanteM.Actualizar(new Participante
            {
                Nombre=txtNombre.Text,
                Idparticipante=int.Parse(txtModificar.Text)
            });
            txtModificar.Visible = false;
            lblModificar.Visible = false;
            ActText(false, false, false, false, false, false,false);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar el participante actual", "Eliminar participante", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                MessageBox.Show("Participante eliminado");
            }
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            Buscar(txtBuscar.Text);
        }

        private void dgvParticipante_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            txtModificar.Visible = true;
            lblModificar.Visible = true;
            ActBoton(true, true);
            ActText(false, true, false, false, false, false,false);
        }

        private void FrmParticipante_Load(object sender, EventArgs e)
        {
            ActBoton(false, false);
            btnNuevo.Visible = false;
            ActText(false, false, false, false, false, false,false);
            txtModificar.Visible = false;
            lblModificar.Visible = false;
            limpiar();
        }
    }
}
