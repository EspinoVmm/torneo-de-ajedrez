﻿using System;
using System.Windows.Forms;
using LogicaNegocio;
using EntdadesAjedrez;

namespace Presentación
{
    public partial class FrmJugador : Form
    {
        private Jugador _jugador;
        private JugadorManejador _jugadorM;
        public FrmJugador()
        {
            InitializeComponent();
            _jugador = new Jugador();
            _jugadorM = new JugadorManejador();  
        }
        private void Buscar(string nombre)
        {
            dgvJugador.DataSource = _jugadorM.Obtener(nombre);
        }
        private void ActBoton(Boolean eliminar, Boolean editar)
        {
            btnEliminar.Enabled = eliminar;
            btnEditar.Enabled = editar;
        }
        private void limpiar()
        {
            txtNivel.Text = "";
            txtNParticipante.Text = "";
            txtIdJugador.Text = "";
            txtID.Text = "";
        }
        private void Eliminar()
        {
            var idjugador = dgvJugador.CurrentRow.Cells["id_jugador"].Value.ToString();
            _jugadorM.Eliminar(idjugador);
        }
        private void ActText(Boolean nombre, Boolean direccion)
        {
            txtNParticipante.Enabled = nombre;
            txtNivel.Enabled = direccion;
        }
        private void GuardarJ()
        {
            Jugador jugador = new Jugador();
            jugador.FkidParticipante = int.Parse(txtNParticipante.Text);
            jugador.Nivel = txtNivel.Text;
            var valida = _jugadorM.Validar(jugador);
            if (valida.Item1)
            {
                _jugadorM.Guardar(jugador);
                MessageBox.Show("El Jugador se guardo correctamente");
            }
            else
            {
                MessageBox.Show(valida.Item2, "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            btnNuevo.Visible = false;
            btnAgregar.Visible = true;
            ActText(false, false);
            GuardarJ();
            limpiar();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            btnNuevo.Visible = true;
            btnAgregar.Visible = false;
            ActText(true, true);
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            _jugadorM.Actualizar(new Jugador
            {
                Nivel= txtNivel.Text,
                Id_jugador= int.Parse(txtIdJugador.Text)
            });
            txtIdJugador.Visible = false;
            lblJugador.Visible = false;
            ActText(false, false);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar el Jugador actual", "Eliminar Jugador", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                MessageBox.Show("Jugador eliminado");
            }
        }

        private void FrmJugador_Load(object sender, EventArgs e)
        {
            ActBoton(false, false);
            btnNuevo.Visible = false;
            ActText(false, false);
            txtIdJugador.Visible = false;
            lblJugador.Visible = false;
            limpiar();
        }

        private void txtID_TextChanged(object sender, EventArgs e)
        {
            Buscar(txtID.Text);
        }

        private void dgvJugador_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            txtIdJugador.Visible = false;
            lblJugador.Visible = false;
            ActBoton(true, true);
            ActText(true, false);
        }
    }
}
