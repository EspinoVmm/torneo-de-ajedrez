﻿using System;
using System.Windows.Forms;
using LogicaNegocio;
using EntdadesAjedrez;

namespace Presentación
{
    public partial class FrmSala : Form
    {
        private Sala _sala;
        private SalaManejador _salaM;
        public FrmSala()
        {
            InitializeComponent();
            _sala = new Sala();
            _salaM = new SalaManejador();
        }
        private void Buscar(string nombre)
        {
            dgvSala.DataSource = _salaM.Obtener(nombre);
        }
        private void ActBoton(Boolean eliminar, Boolean editar)
        {
            btnEliminar.Enabled = eliminar;
            btnEditar.Enabled = editar;
        }
        private void limpiar()
        {
            txtBuscar.Text = "";
            txtCapacidad.Text = "";
            txtHotel.Text = "";
            txtMedio.Text = "";
            txtModificar.Text = "";
            txtNSala.Text = "";
        }
        private void Eliminar()
        {
            var id = dgvSala.CurrentRow.Cells["codigo_sala"].Value.ToString();
            _salaM.Eliminar(id);
        }
        private void ActText(Boolean nsala, Boolean medio, Boolean capacidad , Boolean hotel)
        {
            txtCapacidad.Enabled = capacidad;
            txtHotel.Enabled = hotel;
            txtMedio.Enabled = medio;
            txtNSala.Enabled = nsala;
        }
        private void Guardar()
        {
            Sala sala = new Sala();
            sala.Capacidad = int.Parse(txtCapacidad.Text);
            sala.Medio = txtMedio.Text;
            sala.FkidHotel = int.Parse(txtHotel.Text);
            sala.Codigo_sala = txtNSala.Text;
            var valida = _salaM.Validar(sala);
            if (valida.Item1)
            {
                _salaM.Guardar(sala);
                MessageBox.Show("la zona sala se guardo correctamente");
            }
            else
            {
                MessageBox.Show(valida.Item2, "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void dgvReserva_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            txtModificar.Visible = true;
            lblModificar.Visible = true;
            ActBoton(true, true);
            ActText(true, false, false, false);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            btnNuevo.Visible = false;
            btnAgregar.Visible = true;
            ActText(false, false, false, false);
            Guardar();
            limpiar();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            btnNuevo.Visible = true;
            btnAgregar.Visible = false;
            ActText(true, true, true, true);
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            _salaM.Actualizar(new Sala
            {
                Codigo_sala=txtNSala.Text,
                IdSala=int.Parse(txtModificar.Text)
            });
            txtModificar.Visible = false;
            lblModificar.Visible = false;
            ActText(false, false, false, false);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar la Sala actual", "Eliminar Sala", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                MessageBox.Show("Sala eliminado");
            }
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            Buscar(txtBuscar.Text);
        }
        private void Sala_Load(object sender, EventArgs e)
        {
            ActBoton(false, false);
            btnNuevo.Visible = false;
            ActText(false, false, false, false);
            txtModificar.Visible = false;
            lblModificar.Visible = false;
            limpiar();
        }
    }
}
