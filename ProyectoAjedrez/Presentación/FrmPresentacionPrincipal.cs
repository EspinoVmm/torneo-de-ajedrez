﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Presentación
{
    public partial class FrmPresentacionPrincipal : Form
    {
        public FrmPresentacionPrincipal()
        {
            InitializeComponent();
        }
        
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);


        private void BarraTitulo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (MenuVertical.Width == 250)
            {
                MenuVertical.Width = 70;
            }
            else
                MenuVertical.Width = 250;
        }

        private void Cerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Maximizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            Minimizar.Visible = true;
            Maximizar.Visible = false;
        }

        private void Minimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            Minimizar.Visible = false;
            Maximizar.Visible = true;
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new FrmAlbrito());
        }


        private void AbrirFormEnPanel(object Formhijo)
        {
            if (this.panelContenedor.Controls.Count > 0)
                this.panelContenedor.Controls.RemoveAt(0);
            Form fh = Formhijo as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.panelContenedor.Controls.Add(fh);
            this.panelContenedor.Tag = fh;
            fh.Show();
        }

        private void btnHotel_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new FrmHotel());
        }

        private void btnSala_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new FrmSala());
        }

        private void bnjugador_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new FrmJugador());
        }

        private void btnpartida_Click_1(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new FrmPartida());
        }


        private void btnpais_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new FrmPais());
        }

        private void bntmovimientos_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new FrmMovimientos());
        }

        private void btnreserva_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new FrmReserva());
        }

        private void btnParticipantes_Click_1(object sender, EventArgs e)
        {
            AbrirFormEnPanel(new FrmParticipante());
        }

    }
}
