﻿using System;
using System.Windows.Forms;
using LogicaNegocio;
using EntdadesAjedrez;

namespace Presentación
{

    public partial class FrmHotel : Form
    {
        private HotelManejador _hotelM;
        private Hotel _hotel;
        public FrmHotel()
        {
            InitializeComponent();
            _hotelM = new HotelManejador();
            _hotel = new Hotel();
        }
        private void BuscarHotel(string nombre)
        {
            dgvHotel.DataSource = _hotelM.Obtener(nombre);
        }
        private void ActBoton(Boolean eliminar, Boolean editar)
        {
            btnEliminar.Enabled = eliminar;
            btnEditar.Enabled = editar;

        }
        private void limpiar()
        {
            txtDireccion.Text = "";
            txtNombre.Text = "";
            txtTelefono.Text = "";
        }
        private void Eliminar()
        {
            var idusuario = dgvHotel.CurrentRow.Cells["nombre"].Value.ToString();
            _hotelM.Eliminar(idusuario);

        }
        private void ActText(Boolean nombre,Boolean direccion,Boolean telefono)
        {
            txtNombre.Enabled = nombre;
            txtDireccion.Enabled = direccion;
            txtTelefono.Enabled =telefono;
        }
        private void GuardarH()
        {
            Hotel hotel = new Hotel();
            hotel.Nombre = txtNombre.Text;
            hotel.Direccion = txtDireccion.Text;
            hotel.Telefono = txtTelefono.Text;
            var valida = _hotelM.Validar(hotel);
            if (valida.Item1)
            {
                _hotelM.Guardar(hotel);
                MessageBox.Show("El Hotel se guardo correctamente");
            }
            else
            {
                MessageBox.Show(valida.Item2, "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            btnNuevo.Visible = true;
            btnAgregar.Visible = false;
            ActText(true,true,true);
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            btnNuevo.Visible = false;
            btnAgregar.Visible = true;
            ActText(false, false, false);
            GuardarH();
            limpiar();
        }
        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarHotel(txtBuscar.Text);
        }
        private void FrmHotel_Load(object sender, EventArgs e)
        {
            ActBoton(false,false);
            btnNuevo.Visible = false;
            ActText(false, false, false);
            txtID.Visible = false;
            lblId.Visible = false;
            limpiar();
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar el hotel actual", "Eliminar Hotel", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                MessageBox.Show("Hotel eliminado");
            }
        }
        private void btnEditar_Click(object sender, EventArgs e)
        {
            
            _hotelM.Actualizar(new Hotel
            {                
                Nombre = txtNombre.Text,
                Idhotel = int.Parse(txtID.Text)
            }) ;
            txtID.Visible = false;
            lblId.Visible = false;
            ActText(false, false, false);
        }
        private void dgvHotel_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ActBoton(true, true);
            txtID.Visible = true;
            lblId.Visible = true;
            ActText(true, false, false);
        }
    }
}
