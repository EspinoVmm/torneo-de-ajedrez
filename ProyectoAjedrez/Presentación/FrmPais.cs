﻿using System.Windows.Forms;
using LogicaNegocio;
using EntdadesAjedrez;
using System;

namespace Presentación
{
    public partial class FrmPais : Form
    {
        private PaisManejador _paisM;
        private Pais _Pais;
        public FrmPais()
        {
            InitializeComponent();
            _Pais = new Pais();
            _paisM = new PaisManejador();
        }
        private void Buscar(string nombre)
        {
            dgvPais.DataSource = _paisM.Obtener(nombre);
        }
        private void ActBoton(Boolean eliminar, Boolean editar)
        {
            btnEliminar.Enabled = eliminar;
            btnEditar.Enabled = editar;
        }
        private void limpiar()
        {
            txtBuscar.Text = "";
            txtModificar.Text = "";
            txtNClub.Text = "";
            txtNombre.Text = "";
        }
        private void Eliminar()
        {
            var id = dgvPais.CurrentRow.Cells["Nombre"].Value.ToString();
            _paisM.Eliminar(id);
        }
        private void ActText( Boolean club,Boolean nombre)
        {
            txtNClub.Enabled =club;
            txtNombre.Enabled =nombre;
        }
        private void Guardar()
        {
            Pais pais=new Pais();
            pais.N_Clubs = int.Parse(txtNClub.Text);
            pais.Nombre = txtNombre.Text;
            var valida = _paisM.Validar(pais);
            if (valida.Item1)
            {
                _paisM.Guardar(pais);
                MessageBox.Show("El Pais se guardo correctamente");
            }
            else
            {
                MessageBox.Show(valida.Item2, "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            btnNuevo.Visible = true;
            btnAgregar.Visible = false;
            ActText( true,true);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            btnNuevo.Visible = false;
            btnAgregar.Visible = true;
            ActText( false,false);
            Guardar();
            limpiar();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            _paisM.Actualizar(new Pais
            {
                Nombre=txtNombre.Text,
                IdPais = int.Parse(txtModificar.Text)
            }) ;
            txtModificar.Visible = false;
            lblModificar.Visible = false;
            ActText(false,false);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar el pais actual", "Eliminar pais", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                MessageBox.Show("Pais eliminado");
            }
        }

        private void FrmPais_Load(object sender, EventArgs e)
        {
            ActBoton(false, false);
            btnNuevo.Visible = false;
            ActText(false,false);
            txtModificar.Visible = false;
            lblModificar.Visible = false;
            limpiar();
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            Buscar(txtBuscar.Text);
        }

        private void dgvPais_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            txtModificar.Visible = true;
            lblModificar.Visible = true;
            ActBoton(true, true);
            ActText(true,false);
        }
    }
}
