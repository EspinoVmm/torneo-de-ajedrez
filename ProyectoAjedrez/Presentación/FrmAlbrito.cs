﻿using System;
using System.Windows.Forms;
using LogicaNegocio;
using EntdadesAjedrez;

namespace Presentación
{
    public partial class FrmAlbrito : Form
    {
        private AlbitroManejador _albitroM;
        private Albrito _albitro;
        public FrmAlbrito()
        {
            InitializeComponent();
            _albitro = new Albrito();
            _albitroM = new AlbitroManejador();
        }
        private void Buscar(string nombre)
        {
            dgvAlbitro.DataSource = _albitroM.Obtener(nombre);
        }
        private void ActBoton(Boolean eliminar, Boolean editar)
        {
            btnEliminar.Enabled = eliminar;
            btnEditar.Enabled = editar;
        }
        private void limpiar()
        {
            txtBuscar.Text = "";
            txtNombre.Text = "";
            txtNombreE.Text = "";
            txtParticipante.Text = "";
        }
        private void Eliminar()
        {
            var idalbitro = dgvAlbitro.CurrentRow.Cells["nombre"].Value.ToString();
            _albitroM.Eliminar(idalbitro);
        }
        private void ActText(Boolean nombre, Boolean participante)
        {
            txtNombre.Enabled = nombre;
            txtParticipante.Enabled = participante;
        }
        private void Guardar()
        {
            Albrito albrito = new Albrito();
            albrito.FkidParticipante = int.Parse(txtParticipante.Text);
            albrito.Nombre = txtNombre.Text;
            var valida = _albitroM.Validar(albrito);
            if (valida.Item1)
            {
                _albitroM.Guardar(albrito);
                MessageBox.Show("El Albitro se guardo correctamente");
            }
            else
            {
                MessageBox.Show(valida.Item2, "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            Buscar(txtBuscar.Text);
        }
        private void dgvAlbitro_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            txtNombreE.Visible = true;
            lblNombreE.Visible = true;
            ActBoton(true, true);
            ActText(true, false);
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            btnNuevo.Visible = true;
            btnAgregar.Visible = false;
            ActText(true, true);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            btnNuevo.Visible = false;
            btnAgregar.Visible = true;
            ActText(false, false);
            Guardar();
            limpiar();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar el Albitro actual", "Eliminar Albitro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                MessageBox.Show("Albitro eliminado");
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            _albitroM.Actualizar(new Albrito
            {
                Nombre = txtNombre.Text,
                Id_Albitro=int.Parse(txtNombreE.Text)
            }); 
            txtNombreE.Visible = false;
            lblNombreE.Visible = false;
            ActText(false, false);
        }

        private void FrmAlbrito_Load(object sender, EventArgs e)
        {
            ActBoton(false, false);
            btnNuevo.Visible = false;
            ActText(false, false);
            txtNombreE.Visible = false;
            lblNombreE.Visible = false;
            limpiar();
        }
    }
}
