﻿using System;
using System.Windows.Forms;
using LogicaNegocio;
using EntdadesAjedrez;
namespace Presentación
{
    public partial class FrmReserva : Form
    {
        private Alojamiento _alojamiento;
        private AlojamientoManejador _alojamientoM;
        public FrmReserva()
        {
            InitializeComponent();
            _alojamiento = new Alojamiento();
            _alojamientoM = new AlojamientoManejador();
        }
        private void time()
        {
            dtpEntrada.Format = DateTimePickerFormat.Custom;
            dtpEntrada.CustomFormat = "yyyy/mm/dd";
            dtpSalida.Format = DateTimePickerFormat.Custom;
            dtpSalida.CustomFormat = "yyyy/mm/dd";
        }
        private void Buscar(string nombre)
        {
            dgvReserva.DataSource = _alojamientoM.Obtener(nombre);
        }
        private void ActBoton(Boolean eliminar, Boolean editar)
        {
            btnEliminar.Enabled = eliminar;
            btnEditar.Enabled = editar;
        }
        private void limpiar()
        {
            txtBuscar.Text = "";
            txtModificar.Text = "";
            txtPatricipante.Text = "";
            txtHotel.Text = "";
            /*dtpEntrada.Value = DateTime.Parse(null);
            dtpSalida.Value = DateTime.Parse(null);*/
        }
        private void Eliminar()
        {
            var id = dgvReserva.CurrentRow.Cells["idaloja"].Value.ToString();
            _alojamientoM.Eliminar(id);
        }
        private void ActText(Boolean entrada, Boolean salida, Boolean participante,Boolean hotel)
        {
            txtHotel.Enabled =hotel;
            txtPatricipante.Enabled =participante;
            dtpEntrada.Enabled =entrada;
            dtpSalida.Enabled =salida;
        }
        private void Guardar()
        {
            Alojamiento alojamiento = new Alojamiento();
            alojamiento.Fecha_entrada = dtpEntrada.Value;
            alojamiento.Fecha_salida =  dtpSalida.Value;
            alojamiento.FkidParticipante = int.Parse(txtPatricipante.Text);
            alojamiento.FkidHotel = int.Parse(txtPatricipante.Text);
            var valida = _alojamientoM.Validar(alojamiento);
            if (valida.Item1)
            {
                _alojamientoM.Guardar(alojamiento);
                MessageBox.Show("Se Reservo correctamente");
            }
            else
            {
                MessageBox.Show(valida.Item2, "Ocurrio un error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FrmReserva_Load(object sender, EventArgs e)
        {
            ActBoton(false, false);
            btnNuevo.Visible = false;
            ActText(false, false, false,false);
            txtModificar.Visible = false;
            lblModificar.Visible = false;
            limpiar();
            time();
        }

        private void dgvReserva_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            txtModificar.Visible = true;
            lblModificar.Visible = true;
            ActBoton(true, true);
            ActText(false, false,true, false);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            btnNuevo.Visible = false;
            btnAgregar.Visible = true;
            ActText(false, false, false,false);
            Guardar();
            limpiar();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            btnNuevo.Visible = true;
            btnAgregar.Visible = false;
            ActText(true, true, true,true);
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            _alojamientoM.Actualizar(new Alojamiento
            {
                FkidParticipante=int.Parse(txtPatricipante.Text),
                Idaloja=int.Parse(txtModificar.Text)
            });
            txtModificar.Visible = false;
            lblModificar.Visible = false;
            ActText(false, false, false,false);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea eliminar la Reserva actual", "Eliminar reserva", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Eliminar();
                MessageBox.Show("Reserva eliminado");
            }
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            Buscar(txtBuscar.Text);
        }
    }
}
