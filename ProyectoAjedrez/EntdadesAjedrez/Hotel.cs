﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntdadesAjedrez
{
    public class Hotel
    {
        private int idhotel;
        private string nombre;
        private string direccion;
        private string telefono;

        public int Idhotel { get => idhotel; set => idhotel = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Direccion { get => direccion; set => direccion = value; }
        public string Telefono { get => telefono; set => telefono = value; }
        
    }
}
