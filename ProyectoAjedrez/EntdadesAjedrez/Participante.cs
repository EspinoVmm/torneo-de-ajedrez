﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntdadesAjedrez
{
    public class Participante
    {
        private int idparticipante;
        private string n_social;
        private string nombre;
        private string direccion;
        private string nombreCampeonato;
        private string tipoCampeonato;
        private string telefono;
        private int fkidPais;

        public int Idparticipante { get => idparticipante; set => idparticipante = value; }
        public string N_social { get => n_social; set => n_social = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Direccion { get => direccion; set => direccion = value; }
        public string NombreCampeonato { get => nombreCampeonato; set => nombreCampeonato = value; }
        public string TipoCampeonato { get => tipoCampeonato; set => tipoCampeonato = value; }
        public string Telefono { get => telefono; set => telefono = value; }
        public int FkidPais { get => fkidPais; set => fkidPais = value; }
    }
}
