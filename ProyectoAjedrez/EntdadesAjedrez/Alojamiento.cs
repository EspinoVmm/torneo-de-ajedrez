﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntdadesAjedrez
{
     public class Alojamiento
    {
        private int idaloja;
        private DateTime fecha_entrada;
        private DateTime fecha_salida;
        private int fkidParticipante;
        private int fkidHotel;

        public int Idaloja { get => idaloja; set => idaloja = value; }
        public DateTime Fecha_entrada { get => fecha_entrada; set => fecha_entrada = value; }
        public DateTime Fecha_salida { get => fecha_salida; set => fecha_salida = value; }
        public int FkidParticipante { get => fkidParticipante; set => fkidParticipante = value; }
        public int FkidHotel { get => fkidHotel; set => fkidHotel = value; }
    }
}
