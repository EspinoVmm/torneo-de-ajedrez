﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntdadesAjedrez
{
    public class Jugador
    {
        private int id_jugador;
        private string nivel;
        private int fkidParticipante;

        public int Id_jugador { get => id_jugador; set => id_jugador = value; }
        public string Nivel { get => nivel; set => nivel = value; }
        public int FkidParticipante { get => fkidParticipante; set => fkidParticipante = value; }
    }
}
