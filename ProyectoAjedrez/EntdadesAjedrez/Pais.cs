﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntdadesAjedrez
{
    public class Pais
    {
        private int idPais;
        private string nombre;
        private int n_Clubs;

        public int IdPais { get => idPais; set => idPais = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public int N_Clubs { get => n_Clubs; set => n_Clubs = value; }
        
    }
}
