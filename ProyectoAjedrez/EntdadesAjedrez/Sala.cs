﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntdadesAjedrez
{
    public class Sala
    {
        private int idSala;
        private string codigo_sala;
        private string medio;
        private int capacidad;
        private int fkidHotel;

        public int IdSala { get => idSala; set => idSala = value; }
        public string Codigo_sala { get => codigo_sala; set => codigo_sala = value; }
        public string Medio { get => medio; set => medio = value; }
        public int Capacidad { get => capacidad; set => capacidad = value; }
        public int FkidHotel { get => fkidHotel; set => fkidHotel = value; }
    }
}
