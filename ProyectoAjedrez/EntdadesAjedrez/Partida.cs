﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntdadesAjedrez
{
    public class Partida
    {
        private int idpartida;
        private int codigo_partida;
        private int dia;
        private int mes;
        private int año;
        private int fkidsala;

        public int Idpartida { get => idpartida; set => idpartida = value; }
        public int Codigo_partida { get => codigo_partida; set => codigo_partida = value; }
        public int Dia { get => dia; set => dia = value; }
        public int Mes { get => mes; set => mes = value; }
        public int Año { get => año; set => año = value; }
        public int Fkidsala { get => fkidsala; set => fkidsala = value; }
    }
}
