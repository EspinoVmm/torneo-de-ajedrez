﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntdadesAjedrez
{
    public class Movimiento
    {
        private int id_movimiento;
        private string jugada;
        private int movimiento;
        private string comentario;
        private int fk_idPartida;

        public int Id_movimiento { get => id_movimiento; set => id_movimiento = value; }
        public string Jugada { get => jugada; set => jugada = value; }
        public int Movimiento1 { get => movimiento; set => movimiento = value; }
        public string Comentario { get => comentario; set => comentario = value; }
        public int Fk_idPartida { get => fk_idPartida; set => fk_idPartida = value; }
    }
}
