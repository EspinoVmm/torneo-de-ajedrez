﻿using System;
using System.Data;
using MySql.Data.MySqlClient;

namespace ConexiónBD
{
    public class ConexiónBaseDatos
    {
        private MySqlConnection _con;
        public ConexiónBaseDatos(string server,string user, string password, string database, uint port)
        {
            MySqlConnectionStringBuilder cadenaConexion = new MySqlConnectionStringBuilder();

            cadenaConexion.Server = server;
            cadenaConexion.UserID = user;
            cadenaConexion.Password = password;
            cadenaConexion.Database = database;
            cadenaConexion.Port = port;

            _con = new MySqlConnection(cadenaConexion.ToString());

        }
        public void EjecutarConsulta(string consulta)
        {
            _con.Open();
            var command = new MySqlCommand(consulta, _con);
            command.ExecuteNonQuery();
            _con.Close();
        }

        public DataSet ObtenerDatos(string consulta, string tabla)
        {
            var ds = new DataSet();
            MySqlDataAdapter da = new MySqlDataAdapter(consulta, _con);
            da.Fill(ds, tabla);
            return ds;
        }
    }
}
