﻿using System;
using System.Collections.Generic;
using System.Data;
using EntdadesAjedrez;

namespace ConexiónBD
{
    public class JugadorAccesoDatos
    {
        #region Constructor
        ConexiónBaseDatos _conexion;
        public JugadorAccesoDatos()
        {
            try
            {
                _conexion = new ConexiónBaseDatos("localhost", "root", "", "ajedrez", 3306);
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }
        #endregion
        #region Insert
        public void GuardarJugador(Jugador jugador)
        {
            try
            {
                string consulta = string.Format("insert into jugador values({0},'{1}','{2}');",jugador.Id_jugador, jugador.Nivel,jugador.FkidParticipante);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }
        #endregion
        #region Update
        public void Actualizarjugador(Jugador jugador)
        {
            try
            {
                string consulta = string.Format("update jugador set nivel = ('{0}') where id_jugador = '{1}';",jugador.Nivel,jugador.Id_jugador);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }
        #endregion
        #region Delet
        public void Eliminarjugador(string id)
        {
            try
            {
                string consulta = string.Format("delete from jugador where id_jugador = '{0}';", id);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }
        #endregion
        #region DataGriedView
        public List<Jugador> ObtenerJugador(string filtro)
        {
            var jugadores = new List<Jugador>();
            var ds = new DataSet();
            string consulta = string.Format("select * from Jugador where Id_jugador like '%{0}%'", filtro);
            ds = _conexion.ObtenerDatos(consulta, "Jugador");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var nueva = new Jugador
                {
                    Id_jugador=(int)row[("Id_jugador")],
                    Nivel=row[("Nivel")].ToString(),
                    FkidParticipante=(int)row[("FkidParticipante")]
                };

                jugadores.Add(nueva);
            }
            return jugadores;
        }
        #endregion
    }
}
