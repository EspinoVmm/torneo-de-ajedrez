﻿using System;
using System.Collections.Generic;
using System.Data;
using EntdadesAjedrez;

namespace ConexiónBD
{
    public class SalaAccesoDatos
    {
        #region Constructor
        ConexiónBaseDatos _conexion;
        public SalaAccesoDatos()
        {
            try
            {
                _conexion = new ConexiónBaseDatos("localhost", "root", "", "ajedrez", 3306);
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }
        #endregion
        #region Insert
        public void GuardarSala(Sala sala)
        {
            try
            {
                string consulta = string.Format("insert into sala values({0},'{1}','{2}','{3}','{4}');",sala.IdSala, sala.Codigo_sala,sala.Medio,sala.Capacidad,sala.FkidHotel);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }
        #endregion
        #region Update
        public void ActualizarSala(Sala sala)
        {
            try
            {
                string consulta = string.Format("update sala set codigo_sala = '{0}' where idsala = '{1}';",sala.Codigo_sala ,sala.IdSala);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }
        #endregion
        #region Delet
        public void EliminarSala(string codigo)
        {
            try
            {
                string consulta = string.Format("delete from sala where codigo_sala = '{0}';", codigo);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }
        #endregion
        #region DataGriedView
        public List<Sala> ObtenerSalas(string filtro)
        {
            var salas = new List<Sala>();
            var ds = new DataSet();
            string consulta = string.Format("select * from sala where codigo_sala like '%{0}%'", filtro);
            ds = _conexion.ObtenerDatos(consulta, "sala");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var nueva = new Sala
                {
                    IdSala=(int)row[("IdSala")],
                    Codigo_sala=row[("Codigo_Sala")].ToString(),
                    Medio= row[("Medio")].ToString(),
                    Capacidad= (int)row[("Capacidad")],
                    FkidHotel= (int)row[("FkidHotel")]
                };
                salas.Add(nueva);
            }
            return salas;
        }
        #endregion
    }
}
