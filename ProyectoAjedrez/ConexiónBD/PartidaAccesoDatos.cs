﻿using System;
using System.Collections.Generic;
using System.Data;
using EntdadesAjedrez;

namespace ConexiónBD
{
    public class PartidaAccesoDatos
    {
        #region Constructor
        ConexiónBaseDatos _conexion;
        public PartidaAccesoDatos()
        {
            try
            {
                _conexion = new ConexiónBaseDatos("localhost", "root", "", "ajedrez", 3306);
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }
        #endregion
        #region Insert
        public void GuardarPartida(Partida partida)
        {
            try
            {
                string consulta = string.Format("insert into partida values({0},'{1}','{2}','{3}','{4}','{5}');", 
                   partida.Idpartida, partida.Codigo_partida,partida.Dia,partida.Mes,partida.Año,partida.Fkidsala);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }
        #endregion
        #region Update
        public void ActualizarPartida(Partida partida)
        {
            try
            {
                string consulta = string.Format("update partida set Codigo_partida = ('{0}') where idpartida = '{1}';",partida.Codigo_partida,partida.Idpartida);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }
        #endregion
        #region Delet
        public void EliminarPartida(string Codigo)
        {
            try
            {
                string consulta = string.Format("delete from partida where codigo_partida = '{0}';", Codigo);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }
        #endregion
        #region DataGriedView
        public List<Partida> ObtenerPartidas(string filtro)
        {
            var partidas = new List<Partida>();
            var ds = new DataSet();
            string consulta = string.Format("select * from partida where Codigo_partida like '%{0}%'", filtro);
            ds = _conexion.ObtenerDatos(consulta, "Partida");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var nueva = new Partida
                {
                    Idpartida = (int)row[("Idpartida")],
                    Codigo_partida = (int)row[("Codigo_Partida")],
                    Dia= (int)row[("Dia")],
                    Mes= (int)row[("Mes")],
                    Año= (int)row[("Año")],
                    Fkidsala= (int)row[("Fkidsala")]
                };
                partidas.Add(nueva);
            }
            return partidas;
        }
        #endregion
    }
}
