﻿using System;
using System.Collections.Generic;
using System.Data;
using EntdadesAjedrez;

namespace ConexiónBD
{
    public class PaisAccesoDatos
    {
        #region Constructor
        ConexiónBaseDatos _conexion;
        public PaisAccesoDatos()
        {
            try
            {
                _conexion = new ConexiónBaseDatos("localhost", "root", "", "ajedrez", 3306);
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }
        #endregion
        #region Insert
        public void GuardarPais(Pais pais)
        {
            try
            {
                string consulta = string.Format("insert into pais values({0},'{1}','{2}');",pais.IdPais,pais.Nombre,pais.N_Clubs );
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }
        #endregion
        #region Update
        public void ActualizarPais(Pais pais)
        {
            try
            {
                string consulta = string.Format("update pais set Nnombre = ('{0}') where idpais = '{1}';",pais.Nombre,pais.IdPais);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }
        #endregion
        #region Delet
        public void EliminarPais(string nombre)
        {
            try
            {
                string consulta = string.Format("delete from pais where nombre = '{0}';", nombre);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }
        #endregion
        #region DataGriedView
        public List<Pais> ObtenerPaises(string filtro)
        {
            var paises = new List<Pais>();
            var ds = new DataSet();
            string consulta = string.Format("select * from Pais where nombre like '%{0}%'", filtro);
            ds = _conexion.ObtenerDatos(consulta, "Pais");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var nueva = new Pais
                {
                    IdPais=(int)row[("IdPais")],
                    Nombre=row[("Nombre")].ToString(),
                    N_Clubs=(int)row[("N_Clubs")]
                };

                paises.Add(nueva);
            }
            return paises;
        }
        #endregion
    }
}
