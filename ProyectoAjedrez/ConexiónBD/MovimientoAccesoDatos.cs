﻿using System;
using System.Collections.Generic;
using System.Data;
using EntdadesAjedrez;

namespace ConexiónBD
{
    public class MovimientoAccesoDatos
    {
        #region Constructor
        ConexiónBaseDatos _conexion;
        public MovimientoAccesoDatos()
        {
            try
            {
                _conexion = new ConexiónBaseDatos("localhost", "root", "", "ajedrez", 3306);
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }
        #endregion
        #region Insert
        public void GuardarMovimiento(Movimiento movimiento)
        {
            try
            {
                string consulta = string.Format("insert into movimiento values({0},'{1}','{2}','{3}','{4}');",movimiento.Id_movimiento,movimiento.Jugada,movimiento.Movimiento1,movimiento.Comentario,movimiento.Fk_idPartida );
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }
        #endregion
        #region Update
        public void ActualizarMovimiento(Movimiento movimiento)
        {
            try
            {
                string consulta = string.Format("update movimiento set nombre = ('{0}') where id_movimiento = '{1}';",movimiento.Comentario,movimiento.Id_movimiento);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }
        #endregion
        #region Delet
        public void EliminarMovimiento(string id)
        {
            try
            {
                string consulta = string.Format("delete from movimiento where id_movimiento = '{0}';", id);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }
        #endregion
        #region DataGriedView
        public List<Movimiento> ObtenerMovimiento(string filtro)
        {
            var movimientos = new List<Movimiento>();
            var ds = new DataSet();
            string consulta = string.Format("select * from movimiento where jugada like '%{0}%'", filtro);
            ds = _conexion.ObtenerDatos(consulta, "Movimiento");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var nueva = new Movimiento
                {
                    Id_movimiento=(int)row[("Id_movimiento")],
                    Jugada = row[("Jugada")].ToString(),
                    Movimiento1= (int)row[("Movimiento")],
                    Comentario=row[("Comentario")].ToString(),
                    Fk_idPartida= (int)row[("Fk_idPartida")]
                };

                movimientos.Add(nueva);
            }
            return movimientos;
        }
        #endregion
    }
}
