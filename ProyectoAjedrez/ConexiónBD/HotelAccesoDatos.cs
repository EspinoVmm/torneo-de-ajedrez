﻿using System;
using System.Collections.Generic;
using System.Data;
using EntdadesAjedrez;

namespace ConexiónBD
{
    public class HotelAccesoDatos
    {
        ConexiónBaseDatos _conexion;
        public HotelAccesoDatos()
        {
            try
            {
                _conexion = new ConexiónBaseDatos ("localhost", "root", "", "ajedrez", 3306);
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }
        public void GuardarHotel(Hotel hotel)
        {
            try
            {
                string consulta = string.Format("insert into hotel values({0},'{1}','{2}','{3}');",hotel.Idhotel, hotel.Nombre,hotel.Direccion,hotel.Telefono );
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }

        public void ActualizarUsuario(Hotel hotel)
        {
            try
            {
                string consulta = string.Format("update hotel set nombre = '{0}' where idhotel = '{1}'",hotel.Nombre,hotel.Idhotel);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }
        public void EliminarUsuario(string nombre)
        {
            try
            {
                string consulta = string.Format("delete from hotel where nombre = '{0}'", nombre);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }
        public List<Hotel> ObtenerUsuarios(string filtro)
        {
            var hoteles = new List<Hotel>();

            var ds = new DataSet();
            string consulta = string.Format("select * from hotel where nombre like '%{0}%'", filtro);
            ds = _conexion.ObtenerDatos(consulta, "hotel");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var nueva = new Hotel
                {
                    Idhotel=(int)row[("Idhotel")],
                    Nombre= row[("Nombre")].ToString(),
                    Direccion= row[("Direccion")].ToString(),
                    Telefono= row[("Telefono")].ToString()
                };

                hoteles.Add(nueva);
            }
            return hoteles;
        }
    }
}