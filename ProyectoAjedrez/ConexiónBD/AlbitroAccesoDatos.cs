﻿using System;
using System.Collections.Generic;
using System.Data;
using EntdadesAjedrez;

namespace ConexiónBD
{
    public class AlbitroAccesoDatos
    {
        ConexiónBaseDatos _conexion;
        public AlbitroAccesoDatos()
        {
            try
            {
                _conexion = new ConexiónBaseDatos("localhost", "root", "", "ajedrez", 3306);
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }
        public void GuardarAlbitro(Albrito albrito)
        {
            try
            {
                string consulta = string.Format("insert into albitro values({0},'{1}','{2}');",albrito.Id_Albitro,albrito.Nombre, albrito.FkidParticipante);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }
        public void ActualizarAlbitro(Albrito albrito)
        {
            try
            {
                string consulta = string.Format("update albitro set nombre = ('{0}') where id_albrito = '{1}';", albrito.Nombre, albrito.Id_Albitro);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }
        public void EliminarAlbitro(string nombre)
        {
            try
            {
                string consulta = string.Format("delete from albitro where nombre = '{0}';", nombre);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }
        public List<Albrito> ObtenerAlbitros(string filtro)
        {
            var arbitros = new List<Albrito>();
            var ds = new DataSet();
            string consulta = string.Format("select * from albitro where nombre like '%{0}%'", filtro);
            ds = _conexion.ObtenerDatos(consulta, "albitro");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var nuevo = new Albrito
                {
                    Id_Albitro = (int)row[("Id_albrito")],
                    Nombre = row["Nombre"].ToString(),
                    FkidParticipante = (int)row[("FkidParticipante")]
                };

                arbitros.Add(nuevo);
            }
            return arbitros;
        }
    }
}
