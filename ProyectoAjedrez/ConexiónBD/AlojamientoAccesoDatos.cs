﻿using System;
using System.Collections.Generic;
using System.Data;
using EntdadesAjedrez;

namespace ConexiónBD
{
    public class AlojamientoAccesoDatos
    {
        #region Constructor
        ConexiónBaseDatos _conexion;
        public AlojamientoAccesoDatos()
        {
            try
            {
                _conexion = new ConexiónBaseDatos("localhost", "root", "", "ajedrez", 3306);
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }
        #endregion

        public void GuardarAlojamiento(Alojamiento alojamiento)
        {
            try
            {
                string consulta = string.Format("insert into aloja values({0},'{1}','{2}','{3}','{4}');",alojamiento.Idaloja, alojamiento.Fecha_entrada,alojamiento.Fecha_salida,alojamiento.FkidParticipante,alojamiento.FkidHotel );
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }


        public void ActualizarAlojamiento(Alojamiento alojamiento)
        {
            try
            {
                string consulta = string.Format("update aloja set fkidparticipante = ('{0}')  where idaloja = '{1}';",alojamiento.FkidParticipante,alojamiento.Idaloja);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }

      
        public void EliminarAlojamiento(string id)
        {
            try
            {
                string consulta = string.Format("delete from aloja where idaloja = '{0}';", id);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }
        public List<Alojamiento> ObtenerAlojamientos(string filtro)
        {
            var alojamientos = new List<Alojamiento>();
            var ds = new DataSet();
            string consulta = string.Format("select * from aloja where FkidParticipante like '%{0}%'", filtro);
            ds = _conexion.ObtenerDatos(consulta, "aloja");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var nueva = new Alojamiento
                {
                    Idaloja = (int)row [("Idaloja")],
                    Fecha_entrada = (DateTime)row[("Fecha_entrada")],
                    Fecha_salida = (DateTime)row[("Fecha_salida")],
                    FkidParticipante = (int)row[("FkidParticipante")],
                    FkidHotel =(int)row [("FkidHotel")]
                };

                alojamientos.Add(nueva);
            }
            return alojamientos;
        }
    }
}
