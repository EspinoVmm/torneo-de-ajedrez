﻿using System;
using System.Collections.Generic;
using System.Data;
using EntdadesAjedrez;

namespace ConexiónBD
{
    public class ParticipanteAccesoDatos
    {
        #region Constructor
        ConexiónBaseDatos _conexion;
        public ParticipanteAccesoDatos()
        {
            try
            {
                _conexion = new ConexiónBaseDatos("localhost", "root", "", "ajedrez", 3306);
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló la conexión " + ex.Message);
            }
        }
        #endregion
        #region Insert
        public void GuardarParticipante(Participante participante)
        {
            try
            {
                string consulta = string.Format("insert into Participante values({0},'{1}','{2}','{3}','{4}','{5}','{6}','{7}');",
                    participante.Idparticipante, participante.N_social,participante.Nombre,
                    participante.Direccion,participante.NombreCampeonato,participante.TipoCampeonato,participante.Telefono,participante.FkidPais);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {

                Console.WriteLine("Falló el guardado " + ex.Message);
            }
        }
        #endregion
        #region Update
        public void ActualizarParticipante(Participante participante)
        {
            try
            {
                string consulta = string.Format("update participante set nombre = '{0}' where idparticipante = '{1}';",participante.Nombre,participante.Idparticipante);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló actualizar " + ex.Message);
            }
        }
        #endregion
        #region Delet
        public void EliminarParticipante(string nombre)
        {
            try
            {
                string consulta = string.Format("delete from participante where nombre = '{0}';", nombre);
                _conexion.EjecutarConsulta(consulta);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Falló la eliminación " + ex.Message);
            }
        }
        #endregion
        #region DataGriedView
        public List<Participante> ObtenerParticipantes(string filtro)
        {
            var participantes = new List<Participante>();
            var ds = new DataSet();
            string consulta = string.Format("select * from participante where nombre like '%{0}%'", filtro);
            ds = _conexion.ObtenerDatos(consulta, "Participante");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var nueva = new Participante
                {
                    Idparticipante = (int)row[("Idparticipante")],
                    N_social = row[("N_social")].ToString(),
                    Nombre =row["Nombre"].ToString(),
                    Direccion= row["direcion"].ToString(),
                    NombreCampeonato= row["NombreCampeonato"].ToString(),
                    TipoCampeonato= row["TipoCampeonato"].ToString(),
                    Telefono= row[("Telefono")].ToString(),
                    FkidPais= (int)row[("FkidPais")]
                };
               participantes.Add(nueva);
            }
            return participantes;
        }
        #endregion
    }
}
