﻿using System;
using System.Collections.Generic;
using ConexiónBD;
using EntdadesAjedrez;

namespace LogicaNegocio
{
    public class PaisManejador
    {
        PaisAccesoDatos _pais = new PaisAccesoDatos();
        private bool ValidarExpr(string dato)
        {
            return true;
        }
        public Tuple<bool, string> Validar(Pais pais)
        {
            bool error = true;
            string cadenaErrores = "";
            if (pais.Nombre == null || pais.Nombre.Length==0)
            {
                cadenaErrores = cadenaErrores + "* El campo Nombre no puede ser vacio \n";
                error = false;
            }
            if (pais.N_Clubs == 0)
            {
                cadenaErrores = cadenaErrores + "* El campo Numero Club no puede ser vacio \n";
                error = false;
            }
            
            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }
        public void Guardar(Pais pais)
        {
            try
            {
                _pais.GuardarPais(pais);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public void Actualizar(Pais pais)
        {
            try
            {
                _pais.ActualizarPais(pais);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public void Eliminar(string nombre)
        {
            try
            {
                _pais.EliminarPais(nombre);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public List<Pais> Obtener(string nombre)
        {
            var paises = _pais.ObtenerPaises(nombre);
            return paises;
        }
    }
}
