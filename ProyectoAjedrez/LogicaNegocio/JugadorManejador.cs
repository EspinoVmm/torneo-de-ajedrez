﻿using System;
using System.Collections.Generic;
using ConexiónBD;
using EntdadesAjedrez;

namespace LogicaNegocio
{
    public class JugadorManejador
    {
        JugadorAccesoDatos _jugador = new JugadorAccesoDatos();
        private bool ValidarExpr(string dato)
        {
            return true;
        }
        public Tuple<bool, string> Validar(Jugador jugador)
        {
            bool error = true;
            string cadenaErrores = "";
            if (jugador.Nivel.Length == 0 || jugador.Nivel == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Nivel no puede ser vacio \n";
                error = false;
            }
            if (jugador.FkidParticipante == 0 )
            {
                cadenaErrores = cadenaErrores + "* El campo id Participante no puede ser vacio \n";
                error = false;
            }
            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }
        public void Guardar(Jugador jugador)
        {
            try
            {
                _jugador.GuardarJugador(jugador);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public void Actualizar(Jugador jugador)
        {
            try
            {
                _jugador.Actualizarjugador(jugador);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public void Eliminar(string nombre)
        {
            try
            {
                _jugador.Eliminarjugador(nombre);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public List<Jugador> Obtener(string nombre)
        {
            var jugadors = _jugador.ObtenerJugador(nombre);
            return jugadors;
        }

    }
}
