﻿using System;
using System.Collections.Generic;
using ConexiónBD;
using EntdadesAjedrez;

namespace LogicaNegocio
{
    public class MovimientoManejador
    {
        MovimientoAccesoDatos _movimiento = new MovimientoAccesoDatos();
        private bool ValidarExpr(string dato)
        {
            return true;
        }
        public Tuple<bool, string> Validar(Movimiento movimiento)
        {
            bool error = true;
            string cadenaErrores = "";
            if (movimiento.Jugada.Length == 0 || movimiento.Jugada == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Nivel no puede ser vacio \n";
                error = false;
            }
            if (movimiento.Movimiento1==0)
            {
                cadenaErrores = cadenaErrores + "* El campo id Participante no puede ser vacio \n";
                error = false;
            }
            if (movimiento.Comentario.Length==0|| movimiento.Comentario==null)
            {
                cadenaErrores = cadenaErrores + "* El campo id Participante no puede ser vacio \n";
                error = false;
            }
            if (movimiento.Fk_idPartida==0)
            {
                cadenaErrores = cadenaErrores + "* El campo id Participante no puede ser vacio \n";
                error = false;
            }
            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }
        public void Guardar(Movimiento movimiento)
        {
            try
            {
                _movimiento.GuardarMovimiento(movimiento);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public void Actualizar(Movimiento movimiento)
        {
            try
            {
                _movimiento.ActualizarMovimiento(movimiento);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public void Eliminar(string nombre)
        {
            try
            {
                _movimiento.EliminarMovimiento(nombre);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public List<Movimiento> Obtener(string nombre)
        {
            var movimientos = _movimiento.ObtenerMovimiento(nombre);
            return movimientos;
        }
    }
}
