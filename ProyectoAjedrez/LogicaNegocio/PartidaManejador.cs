﻿using System;
using System.Collections.Generic;
using ConexiónBD;
using EntdadesAjedrez;

namespace LogicaNegocio
{
    public class PartidaManejador
    {
        PartidaAccesoDatos _partida = new PartidaAccesoDatos();
        private bool ValidarExpr(string dato)
        {
            return true;
        }
        public Tuple<bool, string> Validar(Partida partida)
        {
            bool error = true;
            string cadenaErrores = "";
            if (partida.Codigo_partida == 0)
            {
                cadenaErrores = cadenaErrores + "* El campo Codigo de partida no puede ser vacio \n";
                error = false;
            }
            if (partida.Dia == 0)
            {
                cadenaErrores = cadenaErrores + "* El campo dia no puede ser vacio \n";
                error = false;
            }
            if (partida.Mes == 0 )
            {
                cadenaErrores = cadenaErrores + "* El campo mes no puede ser vacio \n";
                error = false;
            }
            if (partida.Año == 0)
            {
                cadenaErrores = cadenaErrores + "* El campo año no puede ser vacio \n";
                error = false;
            }
            if (partida.Fkidsala == 0)
            {
                cadenaErrores = cadenaErrores + "* El campo id sala no puede ser vacio \n";
                error = false;
            }
            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }
        public void Guardar(Partida partida)
        {
            try
            {
                _partida.GuardarPartida(partida);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public void Actualizar(Partida partida)
        {
            try
            {
                _partida.ActualizarPartida(partida);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public void Eliminar(string nombre)
        {
            try
            {
                _partida.EliminarPartida(nombre);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public List<Partida> Obtener(string nombre)
        {
            var partidas = _partida.ObtenerPartidas(nombre);
            return partidas;
        }
    }
}
