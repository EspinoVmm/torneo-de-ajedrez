﻿using System;
using System.Collections.Generic;
using ConexiónBD;
using EntdadesAjedrez;

namespace LogicaNegocio
{
    public class ParticipanteManejador
    {
        ParticipanteAccesoDatos _participante = new ParticipanteAccesoDatos();
        private bool ValidarExpr(string dato)
        {
            return true;
        }
        public Tuple<bool, string> Validar(Participante participante)
        {
            bool error = true;
            string cadenaErrores = "";
            if (participante.Nombre.Length == 0||participante.Nombre ==null)
            {
                cadenaErrores = cadenaErrores + "* El campo Nombre no puede ser vacio \n";
                error = false;
            }
            
            if (participante.N_social.Length == 0 || participante.N_social == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Numero Social no puede ser vacio \n";
                error = false;
            }
            if (participante.Telefono.Length == 0 || participante.Telefono == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Telefono no puede ser vacio \n";
                error = false;
            }
            
            if (participante.Direccion.Length == 0 || participante.Direccion == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Direccion no puede ser vacio \n";
                error = false;
            }
            if (participante.FkidPais == 0 )
            {
                cadenaErrores = cadenaErrores + "* El campo id pais no puede ser vacio \n";
                error = false;
            }
            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }
        public void Guardar(Participante participante)
        {
            try
            {
                _participante.GuardarParticipante(participante);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public void Actualizar(Participante participante)
        {
            try
            {
                _participante.ActualizarParticipante(participante);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public void Eliminar(string nombre)
        {
            try
            {
                _participante.EliminarParticipante(nombre);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public List<Participante> Obtener(string nombre)
        {
            var participantes = _participante.ObtenerParticipantes(nombre);
            return participantes;
        }
    }
}
