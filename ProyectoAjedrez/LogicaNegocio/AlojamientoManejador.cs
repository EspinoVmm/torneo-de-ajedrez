﻿using System;
using System.Collections.Generic;
using ConexiónBD;
using EntdadesAjedrez;

namespace LogicaNegocio
{
    public class AlojamientoManejador
    {
        AlojamientoAccesoDatos _aloja = new AlojamientoAccesoDatos();
        private bool ValidarExpr(string dato)
        {
            return true;
        }
        public Tuple<bool, string> Validar(Alojamiento alojamiento)
        {
            bool error = true;
            string cadenaErrores = "";
            if (alojamiento.FkidHotel.Equals(0) || alojamiento.FkidHotel.Equals(null))
            {
                cadenaErrores = cadenaErrores + "* El campo id hotel no puede ser vacio \n";
                error = false;
            }
            if (alojamiento.FkidParticipante.Equals(0) || alojamiento.FkidParticipante .Equals(null))
            {
                cadenaErrores = cadenaErrores + "* El campo id participante no puede ser vacio \n";
                error = false;
            }
            if (alojamiento.Fecha_entrada==null)
            {
                cadenaErrores = cadenaErrores + "* El campo fecha entrada no puede ser vacia \n";
                error = false;
            }
            if (alojamiento.Fecha_salida == null)
            {
                cadenaErrores = cadenaErrores + "* El campo fecha salida no puede ser vacia \n";
                error = false;
            }

            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }
        public void Guardar(Alojamiento alojamiento)
        {
            try
            {
                _aloja.GuardarAlojamiento(alojamiento);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public void Actualizar(Alojamiento alojamiento)
        {
            try
            {
                _aloja.ActualizarAlojamiento(alojamiento);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public void Eliminar(string nombre)
        {
            try
            {
                _aloja.EliminarAlojamiento(nombre);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public List<Alojamiento> Obtener(string nombre)
        {
            var alojamientos = _aloja.ObtenerAlojamientos(nombre);
            return alojamientos;
        }
    }
}
