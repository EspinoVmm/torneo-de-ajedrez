﻿using System;
using System.Collections.Generic;
using ConexiónBD;
using EntdadesAjedrez;

namespace LogicaNegocio
{
    public class SalaManejador
    {
        SalaAccesoDatos _sala = new SalaAccesoDatos();
        private bool ValidarExpr(string dato)
        {
            return true;
        }
        public Tuple<bool, string> Validar(Sala sala)
        {
            bool error = true;
            string cadenaErrores = "";
            if (sala.Capacidad == 0)
            {
                cadenaErrores = cadenaErrores + "* El campo Capacidad no puede ser vacio \n";
                error = false;
            }
            if (sala.FkidHotel == 0)
            {
                cadenaErrores = cadenaErrores + "* El campo id hotel no puede ser vacio \n";
                error = false;
            }
            if (sala.Codigo_sala.Length == 0 || sala.Codigo_sala == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Codigo de sala no puede ser vacio \n";
                error = false;
            }
            if (sala.Medio.Length == 0|| sala.Medio == null)
            {
                cadenaErrores = cadenaErrores + "* El campo medio no puede ser vacio \n";
                error = false;
            }
            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }
        public void Guardar(Sala sala)
        {
            try
            {
                _sala.GuardarSala(sala);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public void Actualizar(Sala sala)
        {
            try
            {
                _sala.ActualizarSala(sala);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public void Eliminar(string nombre)
        {
            try
            {
                _sala.EliminarSala(nombre);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public List<Sala> Obtener(string nombre)
        {
            var salas = _sala.ObtenerSalas(nombre);
            return salas;
        }
    }
}
