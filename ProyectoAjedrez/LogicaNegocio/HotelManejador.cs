﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConexiónBD;
using EntdadesAjedrez;

namespace LogicaNegocio
{
    public class HotelManejador
    {
        HotelAccesoDatos _hotel = new HotelAccesoDatos();
        private bool ValidarExpr(string dato)
        {
            return true;
        }
        public Tuple<bool, string> Validar(Hotel hotel)
        {
            bool error = true;
            string cadenaErrores = "";
            if (hotel.Nombre.Length == 0 || hotel.Nombre == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Nombre no puede ser vacio \n";
                error = false;
            }
            if (hotel.Direccion.Length == 0 || hotel.Direccion == null)
            {
                cadenaErrores = cadenaErrores + "* El campo Direccion no puede ser vacio \n";
                error = false;
            }
            if (hotel.Telefono.Length == 0 || hotel.Telefono == null)
            {
                cadenaErrores = cadenaErrores + "* El campo telefono no puede ser vacio \n";
                error = false;
            }
            if (hotel.Telefono.Length<10)
            {
                cadenaErrores = cadenaErrores + "* El campo telefono tiene que ser de 10 digitos \n";
                error = false;
            }
            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }
        public void Guardar(Hotel hotel)
        {
            try
            {
                _hotel.GuardarHotel(hotel);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public void Actualizar(Hotel hotel)
        {
            try
            {
                _hotel.ActualizarUsuario(hotel);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public void Eliminar(string nombre)
        {
            try
            {
                _hotel.EliminarUsuario(nombre);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public List<Hotel> Obtener(string nombre)
        {
            var hoteles = _hotel.ObtenerUsuarios(nombre);
            return hoteles;
        }
    }
}
