﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntdadesAjedrez;
using ConexiónBD;

namespace LogicaNegocio
{
    public class AlbitroManejador
    {
        AlbitroAccesoDatos _albitro = new AlbitroAccesoDatos();
        private bool ValidarExpr(string dato)
        {
            return true;
        }
        public Tuple<bool, string> Validar(Albrito albrito)
        {
            bool error = true;
            string cadenaErrores = "";
            if (albrito.Nombre.Length==0||albrito.Nombre ==null)
            {
                cadenaErrores = cadenaErrores + "* El campo nombre no puede ser vacio \n";
                error = false;
            }
            if (albrito.FkidParticipante == 0)
            {
                cadenaErrores = cadenaErrores + "* El campo id participante no puede ser vacio \n";
                error = false;
            }
            var valida = new Tuple<bool, string>(error, cadenaErrores);
            return valida;
        }
        public void Actualizar(Albrito albrito)
        {
            try
            {
                _albitro.ActualizarAlbitro(albrito);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public void Eliminar(string nombre)
        {
            try
            {
                _albitro.EliminarAlbitro(nombre);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        public void Guardar(Albrito albrito)
        {
            try
            {
                _albitro.GuardarAlbitro(albrito);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            } 
        }
        public List<Albrito> Obtener(string nombre)
        {
            var albitros=_albitro.ObtenerAlbitros(nombre);
            return albitros;
        }
    }
}
